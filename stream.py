#!/usr/bin/env python
from __future__ import absolute_import, print_function

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy import API
import json

class MediaListener(StreamListener):
    def __init__(self, stream_id, callback):
        self.stream_id = stream_id
        self.cb = callback

    def on_data(self, data):
        d = json.loads(data)
        if(d["user"]["id_str"]==self.stream_id):
            for media in d["entities"]["media"]:
                 self.cb(media["media_url"])
        else:
            print(d["user"]["name"])
        return True

    def on_error(self, status):
        print(status)

def stream(username, callback):
    with open("creds.json", "r") as creds_file:
        creds = json.load(creds_file)
        consumer_key = creds["consumer_key"]
        consumer_secret = creds["consumer_secret"]
        access_token = creds["access_token"]
        access_token_secret = creds["access_token_secret"]
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)

        api = API(auth)
        user_id = api.get_user(screen_name=username).id_str

        l = MediaListener(user_id, callback)
        stream = Stream(auth, l)
        stream.filter(follow=[user_id])

if __name__ == '__main__':
     stream("archillect", print)
