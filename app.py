from tornado import websocket, web, ioloop
import tweetstream
import json

cl = []

class IndexHandler(web.RequestHandler):
    def get(self):
        self.render("index.html")

class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        if self not in cl:
            cl.append(self)

    def on_close(self):
        if self in cl:
            cl.remove(self)

def websocket_msg_callback(data):
    for c in cl:
        c.write_message(data)

app = web.Application([
    (r'/', IndexHandler),
    (r'/static', IndexHandler),
    (r'/ws', SocketHandler),
], static_path="static")

if __name__ == '__main__':
    with open("creds.json") as f:
        creds = json.load(f)

        def callback(msg):
            if( "user" in msg and "id_str" in msg["user"]):
                if( msg["user"]["id_str"]=="2907774137"):
                    if("extended_entities" in msg and "media" in msg["extended_entities"]):
                        for media in msg["extended_entities"]["media"]:
                            if "video_info" in media and "variants" in media["video_info"]:
                                for video in media["video_info"]["variants"]:
                                    if "url" in video:
                                       websocket_msg_callback(video["url"])

                            elif "media_url" in media:
                                websocket_msg_callback(media["media_url"])
                        print(json.dumps(msg, indent=2))
                else:
                    if("name" in msg["user"]):
                        print(msg["user"]["name"])
            return True

        stream = tweetstream.TweetStream(creds)
        stream.fetch("/1.1/statuses/filter.json?follow=2907774137", callback=callback)

        app.listen(8888)
        ioloop.IOLoop.instance().start()
